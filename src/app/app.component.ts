import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Active Patients', url: '/home', icon: 'home' },
    { title: 'Discharged Patients', url: '/cart', icon: 'body' },
    { title: 'Call Ambulance', url: '/productdetails', icon: 'car' },
    { title: 'Logout', url: '', icon: 'log-out' },
  ];
  constructor(
    public alertController: AlertController,
    private navCtrl:NavController,
    private storage: Storage,
  ) {}

  async ngOnInit() {
    // If using a custom driver:
    // await this.storage.defineDriver(MyCustomDriver)
    await this.storage.create();
  }

  logout(){
    this.alertController.create({
      header: 'Logged out from App',
      message: 'Do you want to logout?',
      backdropDismiss: false,
      buttons: [{
        text: 'Stay',
        role: 'cancel',
        handler: () => {
          console.log('Application exit prevented!');
        }
      }, {
        text: 'Logout',
        handler: () => {
          this.storage.clear();
          this.navCtrl.navigateRoot('/login');
        }
      }]
    }).then(alert => {
        alert.present();
    });
  }

}




