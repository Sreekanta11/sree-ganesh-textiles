import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VerticalproductPageRoutingModule } from './verticalproduct-routing.module';

import { VerticalproductPage } from './verticalproduct.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VerticalproductPageRoutingModule
  ],
  declarations: [VerticalproductPage]
})
export class VerticalproductPageModule {}
