import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BanarasiListPageRoutingModule } from './banarasi-list-routing.module';

import { BanarasiListPage } from './banarasi-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BanarasiListPageRoutingModule
  ],
  declarations: [BanarasiListPage]
})
export class BanarasiListPageModule {}
