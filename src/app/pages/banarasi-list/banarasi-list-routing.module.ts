import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BanarasiListPage } from './banarasi-list.page';

const routes: Routes = [
  {
    path: '',
    component: BanarasiListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BanarasiListPageRoutingModule {}
