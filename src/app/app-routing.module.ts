import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () =>
      import('./login/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'register',
    loadChildren: () =>
      import('./login/register/register.module').then(
        (m) => m.RegisterPageModule
      ),
  },
  {
    path: 'forgotpassword',
    loadChildren: () =>
      import('./login/forgotpassword/forgotpassword.module').then(
        (m) => m.ForgotpasswordPageModule
      ),
  },
  {
    path: 'blank',
    loadChildren: () => import('./login/blank/blank.module').then( m => m.BlankPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'pages',
    loadChildren: () => import('./pages/pages.module').then( m => m.PagesPageModule)
  },
  {
    path: 'category-list',
    loadChildren: () => import('./pages/category-list/category-list-routing.module').then( m => m.CategoryListPageRoutingModule)
  },
  {
    path: 'banarasi-list',
    loadChildren: () => import('./pages/banarasi-list/banarasi-list-routing.module').then( m => m.BanarasiListPageRoutingModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home-routing.module').then( m => m.HomePageRoutingModule)
  },
  {
    path: 'productdetails',
    loadChildren: () => import('./pages/productdetails/productdetails-routing.module').then( m => m.ProductdetailsPageRoutingModule)
  },
  {
    path: 'cart',
    loadChildren: () => import('./pages/cart/cart-routing.module').then( m => m.CartPageRoutingModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile-routing.module').then( m => m.ProfilePageRoutingModule)
  },
  {
    path: 'delivery-details',
    loadChildren: () => import('./pages/delivery-details/delivery-details-routing.module').then( m => m.DeliveryDetailsPageRoutingModule)
  },
  {
    path: 'wishlist',
    loadChildren: () => import('./pages/wishlist/wishlist-routing.module').then( m => m.WishlistPageRoutingModule)
  },
  {
    path: 'add-address',
    loadChildren: () => import('./pages/add-address/add-address-routing.module').then( m => m.AddAddressPageRoutingModule)
  },
  {
    path: 'filter',
    loadChildren: () => import('./pages/filter/filter-routing.module').then( m => m.FilterPageRoutingModule)
  },  {
    path: 'reset',
    loadChildren: () => import('./login/reset/reset.module').then( m => m.ResetPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
